WIP

This is a small application to keep an eye on my spendings and personal budgets.

I started working on this thing because I don't want to use 3rd party services
to manage my finances and most of them don't support my own attempts to manage
stuff.

This is also a playground of mine to learn python, so don't expect any clear
structure.

Anyways, I'm happy for any suggestions, ideas or issues your might contribute.

Requirements
- python 2.x
- Django
- pip install telepot

Components
python manage.py runserver
python manage.py telegram

chart.js http://www.chartjs.org
