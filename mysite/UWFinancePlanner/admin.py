# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .models import *

class BuchungAdmin(admin.ModelAdmin):
    list_display = ('beschreibung', 'betrag', 'datum', 'location', 'nutzer', 'tag')
    list_filter = ('nutzer', 'tag')
    ordering = ('-datum',)
    fieldsets = [
        ('Who?',    {'fields': ['nutzer']}),
        ('Booking info?',    {'fields': ['beschreibung', 'location', 'betrag', 'tag', 'datum']})
    ]

admin.site.register(Nutzer, UserAdmin)
admin.site.register(Buchung, BuchungAdmin)
admin.site.register(Tag)
admin.site.register(Tag_regex)
admin.site.register(Location)
