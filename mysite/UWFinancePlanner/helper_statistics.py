import calendar
from .models import Tag

def calculateAverageSpending(budget, year, month, relevantDay):
    result = float(budget) / calendar.monthrange(year, month)[1] * relevantDay
    result = round(result, 2)
    return result

def calculateAverageSpendingToday(budget):
    from datetime import datetime
    calculateAverageSpending(budget, datetime.now().year, datetime.now().month, datetime.now().day)

def getMsg(Nutzer):
    #TODO GET BUDGET
    budget = 0
    allowedAverage = calculateAverageSpendingToday(budget)
    #TODO GET SPENDINGS
    spendingsSoFar = 0
    #TODO DELTA
    delta = allowedAverage - spendingsSoFar
    msg = 'Youve spend ' + spendingsSoFar
    msg = msg + 'You are allowed to spend ' + allowedAverage
    msg = msg + 'Your current dela is ' + delta
    return msg

def calculateCategories(transactions):
    cats = Tag.objects.all()
    dictCat = {}
    for x in cats:
        dictCat[x] = 0
    for transaction in transactions:
        if transaction.tag is not None:
            dictCat[transaction.tag] = dictCat[transaction.tag] + transaction.betrag
    return dictCat
