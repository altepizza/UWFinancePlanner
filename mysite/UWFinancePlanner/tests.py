# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from UWFinancePlanner.models import Nutzer, Buchung, Konto, Category

from UWFinancePlanner import views

from django.utils import timezone

username = 'User'
correct_chatID = '12345'
wrong_chatID = '9876543112345'
accountName = 'TestAccount'
c1Name = 'TestCategory_1'
c2Name = 'TestCategory_2'
color = '255,99,132'
noColor = 'xxx,xxx,xxx'


class CategorieCalculationTestCase(TestCase):
    def setUp(self):
        c1 = Category.objects.create(name = c1Name, rgb = color)
        c2 = Category.objects.create(name = c2Name)
        user = Nutzer.objects.create(name = username, chatID=correct_chatID)
        account = Konto.objects.create(name = accountName, nutzer = user,
            budget = 1000)
        t1 = Buchung.objects.create(konto = account, nutzer = user,
            beschreibung = 'TestDescription', betrag = 15.56,
            herkunft = 'TestOrigin',
            category = c1)
        t2 = Buchung.objects.create(konto = account, nutzer = user,
            beschreibung = 'TestDescription', betrag = 150.56,
            herkunft = 'TestOrigin',
            category = c2)

    def test_test(self):
        firstKonto = Konto.objects.all().first()
        self.assertEqual(firstKonto.name, accountName)

    def test_colorView(self):
        list = Category.objects.all()
        result = views.getCategoryColors(list)
        success = False
        for x in result:
            if x == color:
                success = True
        self.assertTrue(success)

    def test_CategoryNaming(self):
        category = Category.objects.all().first()
        self.assertEqual(category.name, c1Name)

    def test_CategoryNamingWithSuperior(self):
        subCategory = Category.objects.get(name = c2Name)
        category = Category.objects.get(name = c1Name)
        newName = category.__str__() + ' - ' + subCategory.__str__()
        subCategory.superiorCategory = category
        self.assertEqual(subCategory.__str__(), newName)

    def test_CategoryRGBPositive(self):
        category = Category.objects.get(name = c1Name)
        category.rgb = color
        self.assertEqual(category.rgb, color)

    def test_CategoryRGBNegative(self):
        category = Category.objects.get(name = c1Name)
        category.rgb = noColor
        self.assertEqual(category.rgb, noColor)

        #TODO Command gets called but this is not a real test yet
    def test_CategorizingShit(self):
        from django.core.management import call_command
        call_command('categorize')
        print ('HELLO')

        #TODO Telepot booking test
    def test_TelepotPositive(self):
        self.assertTrue(True)

        #TODO Telepot bookt test
    def test_TelepotNegative(self):
        self.assertTrue(True)

        #TODO WTF
    def test_CommandSTub(self):
        self.assertTrue(True)

    def test_getCategoryColors(self):
        self.assertTrue(True)
