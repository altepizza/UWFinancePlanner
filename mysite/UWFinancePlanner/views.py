from datetime import datetime
import json
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .models import Buchung, Nutzer, Tag, Location
from django.shortcuts import redirect
from UWFinancePlanner import helper_statistics, helper_getter
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse

def login_view(request):
    return render(request, 'UWFinancePlanner/login.html')

def loginAction(request):
    user = request.POST['username']
    pw = request.POST['password']
    user = authenticate(username = user, password = pw)
    if user is not None:
        login(request, user)
        target  = '/UWFinance/' + str(user)
        return redirect(target)
    else:
        return render(request, 'UWFinancePlanner/login.html')

def logout_view(request):
    logout(request)
    return render(request, 'UWFinancePlanner/login.html')

@login_required
def index_user(request, user):
    context = getContext(datetime.now().year, datetime.now().month, user)
    return render(request, 'UWFinancePlanner/index.html', context)

@login_required
def index_dateTime(request, user, year, month):
    context = getContext(int(year), int(month), user)
    return render(request, 'UWFinancePlanner/index.html', context)

@login_required
def index(request):
    context = getContext(datetime.now().year, datetime.now().month, Nutzer.objects.all().first())
    return render(request, 'UWFinancePlanner/index.html', context)

def getContext(year, month, user):
    buchungen_list = Buchung.objects.order_by('datum')
    buchungen_filtered = []
    for buchung in buchungen_list:
        if buchung.datum.year == year and buchung.datum.month == month:
            if buchung.nutzer.username == user:
                buchungen_filtered.append(buchung)
    cats = helper_statistics.calculateCategories(buchungen_filtered)
    ub = Nutzer.objects.get(username = user)
    budget_config = ub.budget
    budget = [str(budget_config) for x in range(len(buchungen_filtered))]
    saldo = []
    average = []
    tmp = 0
    yAxis = []
    tooltip = []
    categoryValues = []
    categories = []
    for buch in buchungen_filtered:
        tmp = tmp + buch.betrag
        saldo.append(str(tmp))
        yAxis.append(str(buch.datum))
        tooltip.append(buch.beschreibung)
        average.append(helper_statistics.calculateAverageSpending(budget_config, year, month, buch.datum.day))
    average.append(helper_statistics.calculateAverageSpending(budget_config, year, month, datetime.now().day))
    yAxis.append(str(datetime.now()))
    listCats = cats.keys()
    listCatsVs = cats.values()
    for cat in listCats:
        categories.append(cat.name)
    for value in listCatsVs:
        categoryValues.append(str(value))
    nextMonth = month + 1
    nextYear = year
    prevMonth = month - 1
    if month == 12:
        nextMonth = 1
        nextYear = year + 1
    if month == 1:
        prevYear = year - 1
        prevMonth = 12
    locations = Location.objects.all()
    context = {
        'location_list': locations,
        'spendings_list': buchungen_filtered,
        'user': user,
        'saldo': saldo,
        'budget': budget,
        'yAxis': yAxis,
        'tooltip': json.dumps(tooltip),
        'average': average,
        'prevMonth': prevMonth,
        'nextMonth': nextMonth,
        'prevYear': year,
        'nextYear': nextYear,
        'categories': json.dumps(categories),
        'category_values': categoryValues,
        'categoryColors': helper_getter.getCategoryColors(categories),
    }
    return context

@login_required
def addSpending(request):
    try:
        amount = request.POST['amount']
        what = request.POST['what']
        where = request.POST['where']
        locations = Location.objects.filter(name=where)
        if not locations:
            location = Location(name=where)
            location.save()
        else:
            location = locations[0]
        who = Nutzer.objects.filter(username=request.user)[0]
        newBuchung = Buchung(nutzer = who, beschreibung = what, betrag = amount,
            location = location)
        newBuchung.save()
    except (KeyError):
        return HttpResponse(KeyError)
    return HttpResponseRedirect(reverse('UWFinancePlanner:index', kwargs={'user':request.user}))

def deleteSpending(request, spending_id):
    spending = get_object_or_404(Buchung, pk=spending_id)
    spending.delete()
    return HttpResponseRedirect(reverse('UWFinancePlanner:index', kwargs={'user':request.user}))
