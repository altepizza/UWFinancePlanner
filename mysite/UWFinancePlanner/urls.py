from django.conf.urls import url

from . import views

#TODO rework URL structure
app_name = 'UWFinancePlanner'
urlpatterns = [
    url(r'^$', views.index, name='index2'),
    url(r'^addSpending/$', views.addSpending, name='addSpending'),
    url(r'^deleteSpending/(?P<spending_id>[0-9]+)/$', views.deleteSpending, name='deleteSpending'),
    url(r'^logout$', views.logout_view, name='logout'),
    url(r'^login$', views.login_view, name='login_view'),
    url(r'^loginAction$', views.loginAction, name='loginAction'),
    url(r'^(?P<user>.+)/(?P<year>[0-9]+)/(?P<month>[0-9]+)$', views.index_dateTime, name='index'),
    url(r'^(?P<user>.+)$', views.index_user, name='index'),
]
