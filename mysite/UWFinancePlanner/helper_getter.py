from .models import Tag

#return all the used colors associated to the different categories for the main view
def getCategoryColors(tags):
    tagColors = []
    for tag in tags:
        tag = Tag.objects.filter(name = tag)[0]
        tagColors.append(tag.rgb)
    return tagColors
