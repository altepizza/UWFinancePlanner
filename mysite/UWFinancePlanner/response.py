def sendStatus(msg, nutzer, bot):
    from .setting import token
    from .statistics import calculateAverageSpendingToday
    konto = Konto.objects.get(nutzer = ub)
    budget_config = konto.budget
    averageSoFar = calculateAverageSpendingToday(budget_config)
    answer = 'Average: ' + averageSoFar
    bot.sendMessage(msg['message']['from']['id'], answer)
