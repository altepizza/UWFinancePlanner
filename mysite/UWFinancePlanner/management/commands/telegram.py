from django.core.management.base import BaseCommand, CommandError
import django.db
import time
import re
import decimal
import datetime
from django.core.management import call_command

import logging
formatter = logging.Formatter('[%(asctime)s] {%(lineno)d} %(levelname)s - %(message)s','%m-%d %H:%M:%S')
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.info('Started')

offseta = 0

money_regex = re.compile(r"\d+(,\d+)*\u20ac")
status_regex = re.compile(r"status")

from telepot.loop import MessageLoop
import telepot
from .setting import *
bot = telepot.Bot(token)

class Command(BaseCommand):
    def do(self, msg):
        #Is the message coming from a valid user
        from UWFinancePlanner.models import Nutzer
        all_Nutzer = Nutzer.objects.all()
        for nutzer in all_Nutzer:
            logger.debug('msg id %s', msg['message']['from']['id'])
            logger.debug('nutzer chatID %s', nutzer.chatID)
            if str(msg['message']['from']['id']) == nutzer.chatID:
                logger.info('Message from valid sender received %s', msg['message']['from'])
                somethingMatched = money_regex.match(msg['message']['text'])
                statusMatched = status_regex.match(msg['message']['text'])
                #does the message contain a valid amount of money?
                if somethingMatched is not None:
                    amount = money_regex.match(msg['message']['text']).group(0)
                    if amount is not None:
                        amount = amount.replace(u"\u20ac", u"")
                        d = float(amount.replace(",","."))
                        if d is not None and msg['message']['text'] is not None:
                            self.bookShit(msg, d, nutzer)
                if statusMatched is not None:
                    from UWFinancePlanner import response
                    from UWFinancePlanner import statistics
                    msg = statistics.getMsg(nutzer)
                    response.sendStatus(msg, nutzer, bot)

    def bookShit(self, msg, d, chatNutzer):
        logger.info('Booking started. Amount %s', d)
        from UWFinancePlanner.models import Buchung, Konto, Nutzer
        ko = Konto.objects.filter(nutzer=chatNutzer)[0]
        date = datetime.datetime.fromtimestamp(msg['message']['date'])
        b1 = Buchung(konto=ko, nutzer=chatNutzer, beschreibung=msg['message']['text'],
            betrag=d, herkunft=msg['message']['text'], datum=date)
        b1.save()
        call_command('categorize')
        b1.refresh_from_db()
        if b1.category is not None:
            answer = 'Booked '+ str(d) + u'\u20ac \n Category: '
            if b1.category.superiorCategory is not None:
                answer = answer + b1.category.superiorCategory.name + ' - '
            if b1.category is not None:
                answer = answer + b1.category.name
        else:
            answer = 'Booked '+ str(d) + u'\u20ac' + '\n Category not found'
        bot.sendMessage(msg['message']['from']['id'], answer)

    def back(self):
        global offseta
        while True:
            msgs = bot.getUpdates(offset=offseta)
            if len(msgs) > 0:
                django.db.close_old_connections()
                logger.debug('received new message(s) via telegram')
                for msg in msgs:
                    logger.debug('msg content: %s', msg)
                    offseta = msg['update_id'] + 1
                    logger.debug('new offste: %s',offseta)
                    self.do(msg)
            time.sleep(15)

    def handle(self, *args, **options):
        try:
            self.back()
        except Exception as e:
            logger.error('ERROR! %s', e)
            time.sleep(360)
            self.handle()
