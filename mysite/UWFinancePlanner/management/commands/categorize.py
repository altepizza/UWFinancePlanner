from django.core.management.base import BaseCommand, CommandError
import django.db
import re
import time

from UWFinancePlanner.models import Buchung, Category, Category_regex

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.info('Started categorizing')

class Command(BaseCommand):

    def match(self, transaction):
        regexes = Category_regex.objects.all()
        match = False
        for regex in regexes:
            reg_compiled = re.compile('.*' + regex.regex + '.*', re.IGNORECASE)
            if reg_compiled.match(transaction.beschreibung) or reg_compiled.match(transaction.herkunft):
                logger.info('Match: %s and %s', transaction, regex)
                transaction.category = regex.category
                transaction.save()
                match = True
        if not match:
            logger.warn('No category for %s', transaction)

    def handle(self, *args, **options):
        bookings = Buchung.objects.filter(category__isnull=True)
        for booking in bookings:
            logger.debug('searching for fit for %s', booking)
            self.match(booking)
