# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
import re
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractUser

class Nutzer(AbstractUser):
    chatID = models.CharField(max_length=200, default='0')
    budget = models.DecimalField(default=0, max_digits=20, decimal_places=2)
    #def __str__(self):
        #return self.name

class Tag(models.Model):
    class Meta:
           ordering = ['name']
    rgbVali = RegexValidator(r'^[0-9]+,[0-9]+,[0-9]+$', 'Enter RGB values: 0,0,0 - 255,255,255')
    name = models.CharField(max_length=200)
    rgb = models.CharField(max_length=25, default='0,0,0', validators=[rgbVali])
    superiorCategory = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)
    def __str__(self):
        if self.superiorCategory is None:
            return self.name
        else:
            return self.superiorCategory.name + ' - ' + self.name

class Tag_regex(models.Model):
    regex = models.CharField(max_length=500)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    def __str__(self):
        return self.tag.name + ' - ' + self.regex

class Location(models.Model):
    name = models.CharField(max_length=200)
    latitude = models.DecimalField(max_digits=15, decimal_places=10, default=0)
    longitude = models.DecimalField(max_digits=15, decimal_places=10, default=0)
    def __str__(self):
        return self.name

class Buchung(models.Model):
    nutzer = models.ForeignKey(Nutzer, null=True, default=1, on_delete=models.CASCADE)
    beschreibung = models.CharField(max_length=200)
    betrag = models.DecimalField(max_digits=15, decimal_places=2)
    location = models.ForeignKey(Location, null=True, on_delete=models.CASCADE)
    datum = models.DateTimeField(default=timezone.now)
    tag = models.ForeignKey(Tag, null=True, on_delete=models.CASCADE)
    def __str__(self):
        return self.beschreibung
